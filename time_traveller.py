#!/usr/bin/python3

# Copyright 2020 Francisco Pina Martins <f.pinamartins@gmail.com>
# This file is part of exsitu_sims.
# exsitu_sims is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# structure_threader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with exsitu_sims. If not, see <http://www.gnu.org/licenses/>.

import argparse
import datetime
import json
import random

from dateutil.relativedelta import relativedelta


def arguments_handler():
    """
    Handles argument parsing
    """
    cli = argparse.ArgumentParser(description="Run ex-situ simulations helper")
    cli.add_argument("-i",
                     dest="infile",
                     help="Input file. CSV format, ';' delimited.",
                     default="")
    cli.add_argument("-o",
                     dest="outfile",
                     help="Output file. CSV format, ';' delimited. If left "
                          "empty, or set to '-' writes to STDOUT.",
                     default=None)
    cli.add_argument("-p",
                     dest="pairs",
                     help="Define which 'parental' pairs are matched.",
                     default=None)

    cli_parsed = cli.parse_args()
    # Turns a "-" as outfile to None to be redirected to STDOUT
    if cli_parsed.outfile == "-":
        cli_parsed.outfile = None


    return cli_parsed


def time_traveller(individual):
    """
    Shifts each dated event back one year. Takes a list corresponding to a
    "pedigree" line in the `lineage` format as input and returns the same list
    with the shifted dates.
    """
    bdate = datetime.datetime.strptime(individual[6], "%Y-%m-%d").date()
    individual[6] = (bdate - relativedelta(years=1)).strftime("%Y-%m-%d")
    if individual[7]:
        ddate = datetime.datetime.strptime(individual[7], "%Y-%m-%d").date()
        individual[7] = (ddate - relativedelta(years=1)).strftime("%Y-%m-%d")
    elif bdate + relativedelta(years=8) >= datetime.date.today():
        ddate = None
    else:
        ddate = datetime.date(datetime.date.today().year, 4, 26)
        individual[7] = ddate.strftime("%Y-%m-%d")
        individual[5] = "0"


    return individual


def infile_parser(infile_name):
    """
    Reads an input CSV file and preforms various operations on it
    """
    modified_csv = {}
    with open(infile_name, "r") as infile:
        header = infile.readline().rstrip().split(";")
        modified_csv[header[0]] = header[1:]

        # Set all dates back one year & kills all inds over 8 years of age
        modified_csv = {x.split(";")[0]:
                        time_traveller(x.rstrip().split(";"))[1:]
                        for x in infile}


    return modified_csv


def cub_spawner(pedigree_dict, pairs):
    """
    Generates new individuals based on the input pairs. Takes the pedigree dict
    and the list of pairs as input and returns the pedigree_dict with the new
    cubs added.
    """
    pass


def main():
    """
    Main funtion to organize the entire script
    """
    cli = arguments_handler()

    timeshifted_csv = infile_parser(cli.infile)

    # TODO: create cubs from pairs;
    # TODO: implement mating success;
    # TODO: implement likelihood of death including infant mortality;
    # TODO: adjust the above values due to inbreeding;
    # TODO: add "K" and consequences for going over it;
    # TODO: add effects for older members (eg. less infant mortality if elders are present)
    # TODO: switch ID numbers with names;
    # TODO: add web frontend;
    # TODO: implement file tokens to avoid tampering;
    # TODO: implement test suite;
    # TODO: setup.py for easy deployment;

    try:
        with open(cli.outfile, "w") as outfile:
            for ids, values in timeshifted_csv.items():
                outfile.write(ids + ";" + ";".join(values) + "\n")
    except TypeError:
        for ids, values in timeshifted_csv.items():
            print(ids + ";" + ";".join(values))


if __name__ == "__main__":
    main()
